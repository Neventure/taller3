let num = null;
let oper = null;
let solved = false;

function punto() {
  if (!obtenerNumero().includes(".")) {
    respuesta(".");
  }
}


function respuesta(res) {
  if (solved) {
    limpiar();
  }
  let num = obtenerNumero();
  if (num == 0) {
    document.getElementById("result").textContent = res;
  } else {
    if (num.length <= 20) {
      document.getElementById("result").textContent += res;
    }
  }
}
function operacion(simbol) {
  if (solved) {
    limpiar();
  }
  if (!obtenerNumero()=="") {
    oper = simbol;
    num = obtenerNumero();
    document.getElementById("current-function").textContent = simbol;
    document.getElementById("result").textContent = "";
  }
}
function resolver() {
  if (num !== null && oper !== null && obtenerNumero() !== "") {
    let num2 = obtenerNumero();
    document.getElementById("current-function").textContent = num + oper + num2;
    num = parseFloat(num);
    num2 = parseFloat(num2);
    switch (oper) {
      case "+":
        document.getElementById("result").textContent = num + num2;
        solved = true;
        break;
      case "-":
        document.getElementById("result").textContent = num - num2;
        solved = true;
        break;
      case "X":
        document.getElementById("result").textContent = num * num2;
        solved = true;
        break;
      case "/":
        document.getElementById("result").textContent = num / num2;
        solved = true;
        break;
    }
    if (obtenerNumero()<=20){
        document.getElementById("result").textContent = obtenerNumero().slice(0,15);
    }
  }
}

function limpiar() {
  num = null;
  oper = null;
  solved = false;
  document.getElementById("result").textContent = "";
  document.getElementById("current-function").textContent = "";
}
function obtenerNumero() {
  return document.getElementById("result").textContent;
}
